import numpy as np
import gsd
import gsd.hoomd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input gsd file')
parser.add_argument('-o', type=str, help='output gsd file')
args = parser.parse_args()

igsd = gsd.hoomd.open(args.i, 'rb')
ogsd = gsd.hoomd.open(args.o, 'wb')
snap0 = igsd[0]
N = snap0.particles.N

for snap0 in igsd:
    snap = gsd.hoomd.Snapshot()
    snap.configuration = snap0.configuration
    snap.particles.N = N
    snap.particles.position = snap0.particles.position
    snap.particles.image = snap0.particles.image
    #snap.particles.velocity = snap0.particles.velocity
    snap.particles.charge = snap0.particles.charge
    snap.bonds.group = snap0.bonds.group
    snap.bonds.N = snap0.bonds.N
    snap.bonds.M = snap.bonds.M
    ogsd.append(snap)
igsd.close()
ogsd.close()
