#! /usr/bin/env python3
import numpy as np, argparse
import pandas as pd
import freud
import gsd, gsd.hoomd

###############

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '*'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def get_pols(box, snap, pol_size=20, N_pols=0):
    if N_pols == 0:
        N_pols = int(snap.particles.N/pol_size)
    return np.array(np.split(box.unwrap(snap.particles.position[:int(N_pols*pol_size)],
                                        snap.particles.image[:int(N_pols*pol_size)]), N_pols))


def get_rcm(box, snap, pols=None, pol_size=None, N_pols=0):
    return np.mean(pols[0:], axis=1)


def get_rg2(box, snap, pols=None, pol_size=None, N_pols=0):
    rcm = get_rcm(box, snap, pols, pol_size)
    rcm_repeat = np.repeat(rcm, pols.shape[1], axis=0).reshape(pols.shape)
    rg2 = np.mean(np.sum((pols - rcm_repeat)**2, axis=2), axis=1)
    return rg2


def get_re2(box, snap, pols=None, pol_size=None, N_pols=0):
    re = pols[:, -1, :] - pols[:, 0, :]
    re2 = np.sum(re**2, axis=1)
    return re2


def get_vcm(box, snap, pols=None, pol_size=None, N_pols=0):
    """ returns the velocity of the center of mass
        of polymers.
    """
    if N_pols==0:
        N_pols = int(snap.particles.N/pol_size)
    vpols = np.array(np.split(snap.particles.velocity[:int(N_pols*pol_size)], N_pols))
    return np.mean(vpols, axis=1)


def get_torsional_order(box, snap, pols=None, pol_size=None, N_pols=0):
    r_i_n1 = pols[:, :-2, :]
    r_i = pols[:, 1:-1, :]
    r_i_p1 = pols[:, 2:, :]
    cross1 = np.cross(r_i_n1, r_i, axis=2)
    cross2 = np.cross(r_i, r_i_p1, axis=2)
    costeta = np.sum(cross1*cross2, axis=2)/(np.linalg.norm(cross1, axis=2)*np.linalg.norm(cross2, axis=2))
    res = np.mean(costeta, axis=1)
    return res


func_dict = {
    'rcm': get_rcm,
    'vcm': get_vcm,
    'rg2': get_rg2,
    're2': get_re2,
    'torsional': get_torsional_order
}
###############

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', type=str, help='input gsd file')
    parser.add_argument('-o', type=str, help='output filename')
    parser.add_argument('--mode', type=str, help='rcm, rg2 (default), re2, vcm, torsional or all', dest='mode', default='rg2')
    parser.add_argument('--output-type', help='either hdf or tsv (default)', default='tsv', dest='output_type')
    parser.add_argument('--results', help='mean (default) or polymer: averaging over all polymers or store the result per polymer.', default='mean')
    parser.add_argument('--pol-size', type=int, default=20, help='polymer size', dest='pol_size')
    parser.add_argument('--N-pols', type=int, default=0, help='Number of polymers (if everything is polymer, pass 0 (default))', dest='N_pols')
    parser.add_argument('--t1', type=float, default=0.2)
    parser.add_argument('--t2', type=float, default=1.0)
    parser.add_argument('--step', type=float, default=0)
    args = parser.parse_args()

    # read the input file
    traj = gsd.hoomd.open(args.i, 'rb')
    box = freud.box.Box.from_box(traj[0].configuration.box)

    # read positions
    start = int(len(traj)*args.t1)
    end = int(len(traj)*args.t2)
    if args.step<=0:
        step = 1
    else:
        step = int(args.step*(end-start))
    print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(args.t1, start, args.t2, end))

    # prepare the result dict
    res = dict()
    quants = func_dict.keys()#['rcm', 'vcm', 'rg2', 're2', 'msid']
    for k in quants:
        res[k] = pd.DataFrame()
    #
    # pos = list()
    idummy = 0
    printProgressBar(idummy, end-start, prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
    for t0 in traj[start:end+step:step]:
        pols = get_pols(box, t0, args.pol_size, args.N_pols)
        for k in func_dict.keys():
            if ((k in args.mode.lower()) or ('all' in args.mode.lower())):
                quant_res = func_dict[k](box, t0, pols, args.pol_size, args.N_pols)
                if args.results=='polymer':
                    row_res = [('pol%d' % i, quant_res[i]) for i in np.arange(len(quant_res))]
                else:
                    row_res = [('mean', quant_res.mean())]

                row = [('timestep', t0.configuration.step)] + row_res
                res[k] = res[k].append(dict(row), ignore_index=True)
        printProgressBar(idummy*step, (end-start), prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
        idummy += 1
    for k in res.keys():
        if res[k].shape[0] > 0:
            res[k] = res[k].astype({'timestep': 'int64'})
            res[k].set_index('timestep', inplace=True)
            if args.output_type=='hdf':
                res[k].to_hdf(args.o+'.hdf', key=k)
            else:
                res[k].to_csv(args.o+'-%s.tsv'%k, sep='\t')
