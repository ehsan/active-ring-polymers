import numpy as np
import pandas as pd
import argparse
import gsd
import gsd.hoomd
import freud


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='*'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                     (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def get_pols(box, snap, pol_size=20, N_pols=0, unwrap=True):
    if N_pols == 0:
        N_pols = int(snap.particles.N/pol_size)
    if unwrap:
        res = np.array(
            np.split(box.unwrap(snap.particles.position, snap.particles.image), N_pols))
    else:
        res = np.array(
            np.split(box.make_fractional(snap.particles.position), N_pols))

    return res
    ###################


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', type=str, help='input gsd file')
    parser.add_argument('--pol-size', type=int,
                        help='size of each polymer', dest='pol_size')
    args = parser.parse_args()
    pol_size = args.pol_size
    igsd = gsd.hoomd.open(args.i, 'rb')
    snap0 = igsd[0]
    N = snap0.particles.N
    N_pols = N // pol_size
    box = freud.box.Box.from_box(snap0.configuration.box)

    # Opening output xyz files
    output_files = dict()
    for i in range(N_pols):
        fname0 = args.i[:-4]+'-pol%d' % i+'.xyz'
        output_files[i] = open(fname0, 'w')

    # Loop over all snapshots
    tcomment_line = 'Lattice="{Lx} 0 0 0 {Ly} 0 0 0 {Lz}" Properties=species:S:1:pos:R:3 Time={timestep}\n'
    idummy = 0
    printProgressBar(idummy, len(igsd), prefix='Progress:',
                     suffix='Complete', length=20, fill=r'|')
    for snap0 in igsd:
        box = freud.box.Box.from_box(snap0.configuration.box)
        pols = get_pols(box, snap0, pol_size, N_pols)
        active_ids = snap0.particles.charge[:pol_size] == 1
        for polid in range(N_pols):
            output_files[polid].write('%d\n' % pol_size)
            output_files[polid].write(tcomment_line.format(
                Lx=box.Lx, Ly=box.Ly, Lz=box.Lz, timestep=snap0.configuration.step))
            xyz_data = pd.DataFrame(columns=['type', 'x', 'y', 'z'])
            xyz_data['type'] = ['Passive']*pol_size
            xyz_data['type'].values[active_ids] = 'Active '
            for i, e in enumerate(['x', 'y', 'z']):
                xyz_data[e] = pols[polid][:, i]

            output_files[polid].write(
                xyz_data.to_string(header=False, index=False)+'\n')

        printProgressBar(idummy, len(igsd), prefix='Progress:',
                         suffix='Complete', length=20, fill=r'|')
        idummy += 1

    igsd.close()
    for i in range(N_pols):
        output_files[i].close()
