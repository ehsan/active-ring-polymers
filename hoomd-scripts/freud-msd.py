#! /usr/bin/env python3
import numpy as np
import argparse
import pandas as pd
import freud
import gsd
import gsd.hoomd

###############


def get_pols(box, snap, pol_size=20, N_pols=0):
    if N_pols == 0:
        N_pols = int(snap.particles.N/pol_size)
    return np.array(np.split(box.unwrap(snap.particles.position[:int(N_pols*pol_size)], snap.particles.image[:int(N_pols*pol_size)]), N_pols))
###############


parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input gsd file')
parser.add_argument('-o', type=str, help='output filename')
parser.add_argument('--mode', type=str, help='rcm or all',
                    dest='mode', default='rcm')
parser.add_argument('--pol-size', type=int, default=20,
                    help='polymer size', dest='pol_size')
parser.add_argument('--N-pols', type=int, default=0,
                    help='Number of polymers (if everything is polymer, pass 0 (default))', dest='N_pols')
parser.add_argument('--t1', type=float, default=0.2)
parser.add_argument('--t2', type=float, default=1.0)
args = parser.parse_args()

# read the input file
traj = gsd.hoomd.open(args.i, 'rb')
box = freud.box.Box.from_box(traj[0].configuration.box)

# read positions
start = int(len(traj)*args.t1)
end = int(len(traj)*args.t2)
print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(
    args.t1, start, args.t2, end))
pos = list()
for t0 in traj[start:end]:
    if args.mode == 'rcm':
        pols = get_pols(box, t0, args.pol_size, args.N_pols)
    else:
        pols = get_pols(box, t0, 1, int(args.N_pols*args.pol_size))
    pos.append(np.mean(pols[0:], axis=1))
pos = np.array(pos)

# calc MSD
msd = freud.msd.MSD(box)
msd.compute(positions=pos)

# write the output
df = pd.DataFrame()
windows = np.arange(0, len(msd.msd))
df['snapshot'] = windows
df['MSD'] = msd.msd
df.set_index('snapshot', inplace=True)
#res = np.vstack([windows, msd.msd]).T
#np.savetxt(fname=args.o, X=res, fmt="%i %f", header='snapshot MSD')
df.to_csv(args.o, sep='\t', header=True)
